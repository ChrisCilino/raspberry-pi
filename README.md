## Welcome to LabVIEW Community Edition With A Slice of Raspberry Pi! 

---

## The Goal
This repository contains a set of LabVIEW projects designed to take you from beginner to confident LabVIEW learner / programmer. I'm not attempting to write yet another getting started manual, but rather connect you to current content and resources while also getting you up and running with something fun.
These lessons build on eachother and culminate in an alarm clock running on a Raspberry Pi. 

Once you've gone through all of the lessons you'll be equipped with

1. The ability to access Raspberry PI's GPIO lines with LabVIEW using the [LINX toolkit from LabVIEW MakerHub](https://www.labviewmakerhub.com/doku.php?id=libraries:linx:start)
2. Basic LabVIEW programming syntax.
3. The ability to self teach by being exposed to various online resources.
4. Basic programming structures such as state machines.

---

## Getting Started

### This tutorial assumes 

1. You've already installed LabVIEW Community Edition. If you haven't, visit [GCentral.org](https://www.gcentral.org) to download it. Use the NI getting started videos [LabVIEW Community](https://learn.ni.com/training/resources/1407) and [LabVIEW NXG Community](https://learn.ni.com/training/resources/1408) for a step by step introduction that leads you from the very beginning all they way through getting connecting with your hardware:
2. You're familiar with Git so you can clone down the repo.
3. You've been able to connect to your Raspberry Pi using the LINX Target Configuration Wizard. 


### If you haven't connected to your Raspberry Pi 

Follow the steps in the NI getting started videos. 

* [LabVIEW Community getting started video](https://learn.ni.com/training/resources/1407)
* [LabVIEW NXG Community getting started video](https://learn.ni.com/training/resources/1408)

For advanced trouble shooting on installation see the article on MakerHub for [Manual Installation](https://www.labviewmakerhub.com/doku.php?id=learn:libraries:linx:misc:target-manual-install). I followed these steps and they solved some problems I was running into. 


### Getting The Alarm Clock Software

1. Go to "downloads" in the left hand side of this repo.
2. Download the latest *.nipkg file or *.vip file. The packages with the same version number contain the same source, just packaged using the different package managers. For a comparison between package managers see the [LabVIEW Wiki Package Manager Comparison Page.](https://labviewwiki.org/wiki/Package_Manager_Comparison)
3. Once downloaded double click to install. The lessons are installed to "C:\LabVIEW Community and Raspberry Pi"
4. Alternatively select "Download Repository". 
5. Alternatively clone the repo if you would like to contribute. 

Once you've got the code on disk, go to the "Source" directory. There you'll see a series of lessons. 
In each lesson you'll see a *.lvproj file. Double click on this file and LabVIEW will open.
You'll need to change the IP address of the Raspberry PI to the IP address from step 3 above. To change the IP address

1. Right click "raspberrypi"
2. Select Properties
3. In the General category, update the "IP Address / DNS Name" field.

You'll need to change the IP address for each lesson.

NOTE: this tutorial assumes you have a functional understanding of electrical circuitry and theory. 

---

## Going Through A Lesson

Each lesson has a "Main.vi". You'll find Main.vi either under 

* "My Computer >> <Lesson Name>.lvlib >> Main.vi" 
or 

* "raspberrypi >> <Lesson Name>.lvlib >> Main.vi"

Double click on the Main.vi to view its front panel (aka User Interface). Press "Control + E" to view the code. Each lesson presents LabVIEW syntax with comments on the front panel and block diagram.
Comments often contain hyperlinks you can click on to learn more about a topic.

The lessons build on each other. So the comments and concepts in the previous lesson are not re-presented in the subsequent lesson.  

### Using the Wiki
Some lessons have an associaed Wiki so be sure to check out the wikis from the left hand pane. For examle see [Lesson 8](https://bitbucket.org/ChrisCilino/raspberry-pi/wiki/Lesson%208%20-%20Unit%20Testing)

---

## Hardware I'm Using
For this tutorial I'm using a Raspberry Pi 4. I purchased the 

* [CanaKit Raspberry Pi 4 4GB Starter MAX Kit - 64 GB Edition](https://www.amazon.com/gp/product/B07XPHWPRB/ref=ppx_yo_dt_b_asin_title_o03_s00?ie=UTF8&psc=1) 
* [Freenove Ultimate Starter Kit for Raspberry PI 4 B 3 B+](https://www.amazon.com/gp/product/B06W54L7B5/ref=ppx_yo_dt_b_asin_title_o02_s00?ie=UTF8&psc=1)

Note: There is no affiliation between me and the above.

---
## Getting Help
LabVIEW has a vibrant and enthusiastic community of developers. To learn about the different forums, blogs, etc, check out the 

* [Community Resources on GCentral.org](https://www.gcentral.org/g-community-resources)
* [LINX Forum on ni.com](https://forums.ni.com/t5/LINX/bd-p/linx-toolkit)
* [Makerhub communithy forums](https://www.labviewmakerhub.com/forums/)
* [LavaG Forum for LabVIEW Communty Edition](https://lavag.org/forum/58-labview-community-edition/)

### Bug Reporting or Making Suggestions
If you find a bug or have an idea to improve the alarm clock, file an "issue" from the left hand side of the page. If you'd like to help develop the Alarm Clock please let me know.

### Trouble Shooting Sites I Found Helpful And Tips

1. [LINX Target Manual Installation Process](https://www.labviewmakerhub.com/doku.php?id=learn:libraries:linx:misc:target-manual-install)
2. [SSH Using Windows 10](https://www.raspberrypi.org/documentation/remote-access/ssh/windows10.md)
3. The LINX Target Configuration Wizard kept telling me I was using 0GB of my Linux Partition out of 58GB. After attempting to resize my partition, both through the wizard and the OS itself... I said "nonsense!" I manually installed using #1 above. 
4. Synaptic Package Manager is great for seeing and upgrading package versions through a GUI. [See info here](https://www.lifewire.com/guide-to-synaptic-package-manager-2205707). lvrt20-schroot-20.0.0.3 is the LabVIEW Community Edition Run Time on Debian. Make sure you've subscribed to feeds.labvewmakerhub.com/debian (this is covered in #1).
---

## License
All software in this repository is licensed under the MIT license found [here](https://bitbucket.org/ChrisCilino/raspberry-pi/src/master/Source/License/License.txt).

## About Me
|       |      |
| --------|---------|
| ![Chris Cilino](https://labviewwiki.org/w/images/f/fa/Profile_Picture.png) | I've been using LabVIEW since 2003. I'm a [LabVIEW Champion](http://bit.ly/ChrisCilino_ChampionBadge) and [Certified LabVIEW Architect](http://bit.ly/ChrisCilino_CLABadge). I have a passion to see engineers use thier talents to innovate, making our world a better place for all of us. I believe our tools should hide unnecessary complexity and enable us to use our talents to solve the end problem, and not fight the tools in the process. LabVIEW has proven to be that tool for me. I believe in the craft of software engineering. As a result I led [Cirrus Logic](http://www.cirrus.com) to become a [Certified Center of Excellence](http://www.ni.com/coe) from [National Instruments](http://www.ni.com) and had the opportunity to present my work at the [NIWeek keynote in 2018](https://bit.ly/ChrisCilino_2018Keynote). I believe in mentorship and teaching, not only in software but also for career development. I had the opportunity to present [Everything A Software Engineer Needs To Know That Has Nothing To Do With Software Engineering](http://bit.ly/ChrisCilino_EverythingYouNeedToKnow) at the [2018 Certified LabVIEW Architect Summit](https://events.ni.com/profile/web/index.cfm?PKwebID=0x79554abcd) in Austin, TX.
|






|       |      |
| --------|---------|
| ![GCentral](https://labviewwiki.org/w/images/a/a3/Gcentral_circle_logo.png)  | And I believe in collaboration which is why I founded [GCentral.org](https://www.gcentral.org/) in September 2019. GCentral.org enables collaboration between LabVIEW programmers by allowing us to find and use each other's code, and enables us to easily contribute back to the community. If you're interested in contributing to GCentral.org please visit the pages on [Collaboration](https://www.gcentral.org/support/collaborate), [Sharing](https://www.gcentral.org/support/share) ,and [Donating](https://www.gcentral.org/support/donate)   |






|       |      |
| --------|---------|
|   | Lastly, I believe in good software engineering and healthy team builiding. I founded [PetranWay](https://www.petranway.com) to help software organizations grow thier software expertise, nurture team culture, and build infrastructure so that they produce quality products that meet business objectives. For more information check out [PetranWay](https://www.petranway.com).   |


Please feel free to check out 

* [My Online Presentations](http://bit.ly/ChrisCilino_Presentations)
* [My Free and Open Source Code](http://bit.ly/ChrisCilino_CSuite)
* [My LinkedIn Profile](http://bit.ly/ChrisCilino_LinkedIn)
* [PetranWay](https://www.petranway.com)
